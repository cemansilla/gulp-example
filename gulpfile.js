'use strict';

/**
 * Incluyo dependencias
 */
let gulp = require('gulp');
var concatJs = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

/**
 * Declaración de funciones
 */
// Procesamiento de archivos .js
gulp.task('minify-js', function() {
  return gulp.src(['src/*.js'])      // Buscaré en una carpeta llamada src
  	.pipe(concatJs('scripts.js'))       // Concateno
    .pipe(uglify())                     // Minifico
    .pipe(rename({                      // Agrego subfijo .min
      suffix: ".min"
    }))
    .pipe(gulp.dest('dist/js'));        // Almaceno
});
// Escucha de cambios
gulp.task('watch', function(){
  // Cada vez que se detecten cambios llamo a la función minifi-js declarada anteriormente
  gulp.watch('src/*.js', gulp.series('minify-js'));
});
// Tarea por defecto que ejecuta el procesamiento de archivos y queda a la escucha de cambios
gulp.task('default', gulp.parallel('minify-js', 'watch'));