# Gulp example
Ejemplo de uso de Gulp para concatenar todos los archivos javascript de un directorio en uno solo, minificar ese archivo generado, renombrarlo para agregarle el prefijo '.min' y el resultado guardarlo en un directorio.

El mismo procedimiento quedará a la espera de cambios, cada vez que se guarde el archivo se ejecutará de forma automática, con lo que no tendremos que hacerlo de forma manual cada vez.

## ¿Cómo usar el ejemplo?
1. Asegurarse de cumplir con los requisitos, los pueden ver en https://www.cesarmansilla.com/blog/2019/04/22/gulp-automatizando-procesos/
2. Instalar dependencias `npm install`
3. Ejecutar el comando `gulp` para iniciar el proceso


# A disfrutar!